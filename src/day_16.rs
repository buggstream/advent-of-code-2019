use itertools::Itertools;
use std::error::Error;

pub fn flawed_frequency(digits: &[i64], phases: usize) -> Vec<i64> {
    let pattern = [0, 1, 0, -1];
    let mut cur_digits = Vec::from(digits);
    let mut output_digits = vec![0; digits.len()];

    for _ in 0..phases {
        for output_index in 0..cur_digits.len() / 2 {
            let mut sum_digit = 0;

            for (mut digit_index, digit) in cur_digits[output_index..].iter().enumerate() {
                digit_index += output_index;
                let pattern_index = ((digit_index + 1) / (output_index + 1)) % pattern.len();
                sum_digit += *digit * pattern[pattern_index];
            }

            output_digits[output_index] = sum_digit.abs() % 10;
        }

        let mut cur_sum = 0;

        for output_index in ((cur_digits.len() / 2)..cur_digits.len()).rev() {
            cur_sum += cur_digits[output_index];

            output_digits[output_index] = cur_sum.abs() % 10;
        }

        cur_digits.copy_from_slice(&output_digits);
    }

    cur_digits
}

pub fn flawed_frequency_message(digits: &[i64], repeats: usize, phases: usize) -> String {
    let mut cur_digits = Vec::new();

    for _ in 0..repeats {
        cur_digits.extend_from_slice(digits);
    }

    let mut output_digits = vec![0; cur_digits.len()];
    let offset: usize = cur_digits[..7].iter().join("").parse().unwrap();
    assert!(offset > cur_digits.len() / 2);

    for _ in 0..phases {
        let mut cur_sum = 0;

        for output_index in (offset..cur_digits.len()).rev() {
            cur_sum += cur_digits[output_index];

            output_digits[output_index] = cur_sum.abs() % 10;
        }

        cur_digits.copy_from_slice(&output_digits);
    }

    cur_digits[offset..offset + 8].iter().join("")
}

pub fn parse_digits(digit_str: &str) -> Result<Vec<i64>, Box<dyn Error>> {
    digit_str
        .chars()
        .map(|digit_char| {
            digit_char
                .to_digit(10)
                .ok_or("Invalid digit".into())
                .map(|digit| digit as i64)
        })
        .collect()
}

#[cfg(test)]
mod test {
    use crate::day_16::{flawed_frequency, flawed_frequency_message, parse_digits};
    use crate::util::file_to_string;
    use itertools::Itertools;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let digit_string = file_to_string(File::open("./data/day_16")?)?;
        let digits = parse_digits(&digit_string)?;
        let output = flawed_frequency(&digits, 100).iter().take(8).join("");
        let expected = "50053207";

        assert_eq!(output, expected);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let digit_string = file_to_string(File::open("./data/day_16")?)?;
        let digits = parse_digits(&digit_string)?;
        let output = flawed_frequency_message(&digits, 10000, 100);
        let expected = "32749588";

        assert_eq!(output, expected);

        Ok(())
    }

    #[test]
    fn puzzle_two_simple() -> Result<(), Box<dyn Error>> {
        let digits = parse_digits("03036732577212944063491565474664")?;
        let output = flawed_frequency_message(&digits, 10000, 100);
        let expected = "84462026";

        assert_eq!(output, expected);

        Ok(())
    }
}
