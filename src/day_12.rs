#[cfg(test)]
mod test {
    use crate::gravity_system::systems::Object3DSystem;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_12")?;
        let mut objects3d = Object3DSystem::try_from(data_file)?;
        let total_energy = objects3d.total_energy(1000);

        assert_eq!(total_energy, 14907);

        Ok(())
    }

    #[test]
    fn puzzle_one_simple() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_12_simple")?;
        let mut objects3d = Object3DSystem::try_from(data_file)?;
        let total_energy = objects3d.total_energy(100);

        assert_eq!(total_energy, 1940);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_12")?;
        let mut objects3d = Object3DSystem::try_from(data_file)?;
        let repeat_after = objects3d.repeat_after();

        assert_eq!(repeat_after, 467081194429464);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_12_repeat_small")?;
        let mut objects3d = Object3DSystem::try_from(data_file)?;
        let repeat_after = objects3d.repeat_after();

        assert_eq!(repeat_after, 2772);

        Ok(())
    }
}
