use crate::intcode::process::{SignalInput, SignalOutput};

pub trait InputSignalHandler {
    fn handle_signal(&mut self, input_signal: SignalInput) -> SignalOutput {
        match input_signal {
            SignalInput::Stdin(input_ref) => self.handle_input(input_ref),
            SignalInput::Stdout(output) => self.handle_output(output),
            SignalInput::Halt => self.handle_halt(),
        }
    }

    fn handle_input(&mut self, input_ref: &mut i64) -> SignalOutput;

    fn handle_output(&mut self, output: i64) -> SignalOutput;

    fn handle_halt(&mut self) -> SignalOutput;
}
