use crate::intcode::process::{SignalInput, SignalOutput};
use crate::intcode::processor::Processor;
use crate::intcode::program::Executable;
use std::collections::{HashMap, VecDeque};

#[derive(Debug, Copy, Clone)]
pub enum Direction {
    NORTH,
    SOUTH,
    WEST,
    EAST,
}

impl Direction {
    pub fn direction_between(
        (from_x, from_y): (i64, i64),
        (to_x, to_y): (i64, i64),
    ) -> Option<Direction> {
        let mut direction_x = None;
        let mut direction_y = None;

        if to_x - from_x == 1 {
            direction_x = Some(Direction::EAST);
        } else if to_x - from_x == -1 {
            direction_x = Some(Direction::WEST);
        }

        if to_y - from_y == 1 {
            direction_y = Some(Direction::NORTH);
        } else if to_y - from_y == -1 {
            direction_y = Some(Direction::SOUTH);
        }

        match (direction_x, direction_y) {
            (Some(direction), None) => Some(direction),
            (None, Some(direction)) => Some(direction),
            _ => None,
        }
    }

    pub fn move_from(&self, (x, y): (i64, i64)) -> (i64, i64) {
        match self {
            Direction::NORTH => (x, y + 1),
            Direction::SOUTH => (x, y - 1),
            Direction::EAST => (x + 1, y),
            Direction::WEST => (x - 1, y),
        }
    }
}

impl From<Direction> for i64 {
    fn from(from: Direction) -> Self {
        match from {
            Direction::NORTH => 1,
            Direction::SOUTH => 2,
            Direction::WEST => 3,
            Direction::EAST => 4,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum RepairRobotSignal {
    Continue,
    DetectedOxygenTank,
    ExplorationComplete,
    Halt,
}

#[derive(Debug)]
pub struct RepairRobot {
    start_pos: (i64, i64),
    current_pos: (i64, i64),
    path: VecDeque<Direction>,
    parents: HashMap<(i64, i64), (i64, i64)>,
    to_visit: VecDeque<(i64, i64)>,
}

impl RepairRobot {
    fn new(start_pos: (i64, i64)) -> RepairRobot {
        let mut to_visit = VecDeque::new();
        to_visit.push_back(start_pos);

        RepairRobot {
            start_pos,
            current_pos: start_pos,
            path: VecDeque::new(),
            parents: HashMap::new(),
            to_visit,
        }
    }

    pub fn fewest_steps(processor: &Processor, executable: Executable) -> usize {
        let start = (0, 0);
        let mut repair_robot = RepairRobot::new(start);
        processor.run(executable, &mut |signal_input| match repair_robot
            .find_oxygen_stage(signal_input)
        {
            RepairRobotSignal::Continue => SignalOutput::Continue,
            _ => SignalOutput::Halt,
        });

        repair_robot.backtrack_path(repair_robot.current_pos).len() - 1 // Subtract by one, because the starting position is included
    }

    pub fn oxygen_replenished(processor: &Processor, executable: Executable) -> usize {
        let start = (0, 0);
        let mut repair_robot = RepairRobot::new(start);
        let mut found_oxygen_tank = false;

        processor.run(executable, &mut |signal_input| {
            let signal = match signal_input {
                SignalInput::Stdin(input_ref) => repair_robot.handle_input(input_ref),
                SignalInput::Stdout(output) => repair_robot.handle_output(output),
                SignalInput::Halt => {
                    panic!("The robot is not expected to stop working at this stage!")
                }
            };

            if !found_oxygen_tank && signal == RepairRobotSignal::DetectedOxygenTank {
                found_oxygen_tank = true;
                repair_robot = RepairRobot::new(repair_robot.current_pos);
            }

            match signal {
                RepairRobotSignal::DetectedOxygenTank | RepairRobotSignal::Continue => {
                    SignalOutput::Continue
                }
                RepairRobotSignal::ExplorationComplete => {
                    println!("Exploration complete!");
                    SignalOutput::Halt
                }
                _ => SignalOutput::Halt,
            }
        });

        // Since the robot uses BFS the last position explored is also the furthest
        repair_robot.backtrack_path(repair_robot.current_pos).len() - 1
    }

    fn find_oxygen_stage(&mut self, signal_input: SignalInput) -> RepairRobotSignal {
        match signal_input {
            SignalInput::Stdin(input_ref) => self.handle_input(input_ref),
            SignalInput::Stdout(output) => self.handle_output(output),
            SignalInput::Halt => RepairRobotSignal::Halt,
        }
    }

    fn handle_input(&mut self, input_ref: &mut i64) -> RepairRobotSignal {
        if self.path.len() == 0 {
            for neighbour in self.get_neighbours() {
                self.parents.insert(neighbour, self.current_pos);
                self.to_visit.push_back(neighbour);
            }

            if self.select_next_destination().is_none() {
                return RepairRobotSignal::ExplorationComplete;
            }
        }

        let direction = *self.path.front().unwrap(); // Only remove the direction once the output has been read.
        *input_ref = direction.into();
        RepairRobotSignal::Continue
    }

    fn handle_output(&mut self, output: i64) -> RepairRobotSignal {
        let travel_direction = self.path.pop_front().unwrap();

        if output == 0 {
            assert_eq!(
                self.path.len(),
                0,
                "The robot hit a wall, while it should have been backtracking."
            );

            return RepairRobotSignal::Continue;
        }

        self.current_pos = travel_direction.move_from(self.current_pos);

        match output {
            1 => RepairRobotSignal::Continue,
            2 => RepairRobotSignal::DetectedOxygenTank,
            _ => panic!("Invalid robot status code!"),
        }
    }

    fn backtrack_path(&self, mut from: (i64, i64)) -> Vec<(i64, i64)> {
        let mut path = Vec::new();
        path.push(from);

        while let Some(parent) = self.parents.get(&from) {
            path.push(*parent);
            from = *parent;
        }

        path
    }

    fn combine_paths(
        current_path: &[(i64, i64)],
        destination_path: &[(i64, i64)],
    ) -> Vec<(i64, i64)> {
        let mut combined = Vec::new();

        let common_ancestors = current_path
            .iter()
            .rev()
            .zip(destination_path.iter().rev())
            .take_while(|(left, right)| left == right)
            .count();

        if common_ancestors == 0 {
            return combined;
        }

        let cur_path_length = current_path.len() - common_ancestors;
        let dest_path_length = destination_path.len() - common_ancestors + 1;

        combined.extend(current_path[..cur_path_length].iter());
        combined.extend(destination_path[..dest_path_length].iter().rev());

        combined
    }

    fn select_next_destination(&mut self) -> Option<()> {
        debug_assert!(self.path.len() == 0);

        let destination = loop {
            let destination = self.to_visit.pop_front()?;
            if destination != self.current_pos {
                break destination;
            }
        };

        let dest_path = self.backtrack_path(destination);
        let cur_path = self.backtrack_path(self.current_pos);
        let combined_path = Self::combine_paths(&cur_path, &dest_path);

        self.path.extend(combined_path.windows(2).map(|window| {
            Direction::direction_between(window[0], window[1])
                .expect("Can't move further than one step!")
        }));

        Some(())
    }

    fn get_neighbours(&self) -> Vec<(i64, i64)> {
        let (x, y) = self.current_pos;
        [(x, y + 1), (x + 1, y), (x, y - 1), (x - 1, y)]
            .iter()
            .cloned()
            .filter(|position| !self.parents.contains_key(position) && *position != self.start_pos)
            .collect()
    }
}

#[cfg(test)]
mod test {
    use crate::day_15::RepairRobot;
    use crate::intcode::processor::Processor;
    use crate::intcode::program::Executable;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_15")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let output = RepairRobot::fewest_steps(&processor, executable);

        assert_eq!(output, 374);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_15")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let output = RepairRobot::oxygen_replenished(&processor, executable);

        assert_eq!(output, 482);

        Ok(())
    }
}
