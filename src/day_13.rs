use crate::intcode::process::{SignalInput, SignalOutput};
use crate::intcode::processor::Processor;
use crate::intcode::program::Executable;
use itertools::Itertools;
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::io::{stdout, Write};
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use termion::{clear, cursor};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Tile {
    Empty,
    Wall,
    Block,
    HorizontalPaddle,
    Ball,
}

impl Display for Tile {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{}",
            match self {
                Tile::Empty => ' ',
                Tile::Wall => '█',
                Tile::Block => '□',
                Tile::HorizontalPaddle => '#',
                Tile::Ball => 'o',
            }
        )
    }
}

impl TryFrom<i64> for Tile {
    type Error = Box<dyn Error>;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Tile::Empty),
            1 => Ok(Tile::Wall),
            2 => Ok(Tile::Block),
            3 => Ok(Tile::HorizontalPaddle),
            4 => Ok(Tile::Ball),
            _ => Err("Can't convert this value into a tile!".into()),
        }
    }
}

pub fn block_tiles(processor: &Processor, executable: Executable) -> usize {
    let drawings = collect_drawings(processor, executable);

    drawings
        .values()
        .filter(|tile| **tile == Tile::Block)
        .count()
}

pub fn collect_drawings(
    processor: &Processor,
    executable: Executable,
) -> HashMap<(i64, i64), Tile> {
    let output = processor.run_piped(executable, &[]);
    let mut map = HashMap::new();

    for drawing_cmd in output.chunks_exact(3) {
        let x = drawing_cmd[0];
        let y = drawing_cmd[1];
        let tile: Tile = drawing_cmd[2].try_into().unwrap();

        map.insert((x, y), tile);
    }

    map
}

pub fn run_game(
    processor: &Processor,
    executable: Executable,
    screen_width: usize,
    screen_height: usize,
) -> i64 {
    let mut screen = vec![vec![Tile::Empty; screen_width]; screen_height];
    let mut score = 0;
    let mut stdout = stdout().into_raw_mode().unwrap();
    let mut output_buffer = [0; 3];
    let mut output_buffer_len = 0;

    processor.run(executable, &mut |signal_input| match signal_input {
        SignalInput::Stdin(input_ref) => {
            write!(
                stdout,
                "{}{}{}{}\nscore: {}",
                clear::All,
                cursor::Goto(1, 1),
                cursor::Hide,
                screen.iter().map(|row| row.iter().join("")).join("\r\n"),
                score,
            )
            .unwrap();
            stdout.flush().unwrap();

            let stdin = std::io::stdin();
            let input_direction = match stdin.keys().next().unwrap().unwrap() {
                Key::Left => Some(-1),
                Key::Right => Some(1),
                Key::Char('q') | Key::Ctrl('c') => None,
                _ => Some(0),
            };
            if let Some(input_direction) = input_direction {
                *input_ref = input_direction;
                SignalOutput::Continue
            } else {
                SignalOutput::Halt
            }
        }
        SignalInput::Stdout(stdout) => {
            if output_buffer_len < 3 {
                output_buffer[output_buffer_len] = stdout;
                output_buffer_len += 1;
            }

            if output_buffer_len == 3 {
                let x = output_buffer[0];
                let y = output_buffer[1];

                if x < 0 || y < 0 {
                    score = output_buffer[2];
                } else {
                    let x_coord: usize = x.try_into().unwrap();
                    let y_coord: usize = y.try_into().unwrap();
                    let tile: Tile = output_buffer[2].try_into().unwrap();

                    screen[y_coord][x_coord] = tile;
                }

                output_buffer_len = 0;
            }

            SignalOutput::Continue
        }
        SignalInput::Halt => SignalOutput::Halt,
    });

    write!(stdout, "{}\r\n", cursor::Show).unwrap();
    stdout.suspend_raw_mode().unwrap();
    stdout.flush().unwrap();

    score
}

#[cfg(test)]
mod test {
    use crate::day_13::block_tiles;
    use crate::intcode::processor::Processor;
    use crate::intcode::program::Executable;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_13")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let output = block_tiles(&processor, executable);

        assert_eq!(output, 247);

        Ok(())
    }

    //    #[test]
    //    fn puzzle_two() -> Result<(), Box<dyn Error>> {
    //        let data_file = File::open("./data/day_13")?;
    //        let mut program = OpcodeProgram::try_from(data_file)?;
    //        let processor = Processor::default();
    //
    //        program.store_value(0, 2); // Play for free hack!
    //
    //        let score = run_game(&processor, program, 40, 25);
    //
    //        Ok(())
    //    }
}
