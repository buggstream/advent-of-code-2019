use std::convert::TryFrom;
use std::error::Error;
use std::fs::File;
use std::io::Read;

#[derive(Debug)]
pub struct Image {
    width: u32,
    height: u32,
    pixels: Vec<u8>,
}

impl Image {
    pub fn layer_iter(&self) -> impl Iterator<Item = &[u8]> {
        self.pixels
            .chunks_exact((self.width * self.height) as usize)
    }

    pub fn display_image(&self) -> String {
        let mut image_string = String::new();

        for index in 0..(self.width * self.height) as usize {
            if index != 0 && index % self.width as usize == 0 {
                image_string.push('\n');
            }

            let pixel = self
                .pixel_iter(index)
                .skip_while(|digit| *digit == 2)
                .next()
                .unwrap_or(2);
            image_string.push_str(&pixel.to_string());
        }

        image_string
    }

    fn pixel_iter<'a>(&'a self, start_index: usize) -> impl Iterator<Item = u8> + 'a {
        self.pixels
            .iter()
            .cloned()
            .skip(start_index)
            .step_by((self.width * self.height) as usize)
    }
}

impl TryFrom<(u32, u32, File)> for Image {
    type Error = Box<dyn Error>;

    fn try_from((width, height, mut file): (u32, u32, File)) -> Result<Self, Self::Error> {
        let mut file_input = String::new();
        file.read_to_string(&mut file_input)?;

        let digits: Result<Vec<u8>, &str> = file_input
            .chars()
            .map(|cur_char| {
                cur_char
                    .to_digit(10)
                    .map(|digit| digit as u8)
                    .ok_or("Invalid digit")
            })
            .collect();

        Ok(Image {
            width,
            height,
            pixels: digits?,
        })
    }
}

pub fn layer_with_fewest(
    image: &Image,
    possible_digits: &[u8],
    min_index: usize,
) -> Option<Vec<u64>> {
    image
        .layer_iter()
        .map(|layer| digits_count(layer, possible_digits))
        .min_by_key(|layer_count| layer_count[min_index])
}

fn digits_count(layer: &[u8], digits_to_count: &[u8]) -> Vec<u64> {
    let mut digits_count = vec![0; digits_to_count.len()];

    for digit in layer {
        let index = digits_to_count.iter().position(|compare| compare == digit);

        if let Some(matched_index) = index {
            digits_count[matched_index] += 1;
        }
    }

    digits_count
}

#[cfg(test)]
mod test {
    use crate::day_8::{layer_with_fewest, Image};
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_8")?;
        let possible_digits = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        let image = Image::try_from((25, 6, data_file))?;
        let result = layer_with_fewest(&image, &possible_digits, 0).unwrap();

        println!("{:?}", result);
        assert_eq!(result[1] * result[2], 1215);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_8")?;
        let image = Image::try_from((25, 6, data_file))?;
        let result = image.display_image();
        let display_string = result.replace("0", " ");
        let expected: String = vec![
            "1000010010011001110010010",
            "1000010010100101001010010",
            "1000011110100001001011110",
            "1000010010100001110010010",
            "1000010010100101000010010",
            "1111010010011001000010010",
        ]
        .join("\n");

        println!("{}", display_string);
        assert_eq!(result, expected);

        Ok(())
    }
}
