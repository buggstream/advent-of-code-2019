use crate::intcode::process::{SignalInput, SignalOutput};
use crate::intcode::processor::Processor;
use crate::intcode::program::Executable;
use crate::util::{RobotDirection, RobotMove};
use itertools::{Itertools, MinMaxResult};
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};
use std::error::Error;

#[derive(Debug, Copy, Clone)]
pub enum PaintColor {
    Black,
    White,
}

impl PaintColor {
    pub fn as_char(&self) -> char {
        match self {
            PaintColor::Black => ' ',
            PaintColor::White => '#',
        }
    }
}

impl TryFrom<i64> for PaintColor {
    type Error = Box<dyn Error>;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(PaintColor::Black),
            1 => Ok(PaintColor::White),
            _ => Err("This integer value can not be converted to a color.".into()),
        }
    }
}

impl From<PaintColor> for i64 {
    fn from(color: PaintColor) -> Self {
        match color {
            PaintColor::Black => 0,
            PaintColor::White => 1,
        }
    }
}

pub fn paint_count(processor: &Processor, executable: Executable) -> usize {
    let mut color_map = HashMap::new();
    paint_map(processor, executable, &mut color_map);
    color_map.keys().count()
}

pub fn paint_map(
    processor: &Processor,
    executable: Executable,
    color_map: &mut HashMap<(i64, i64), PaintColor>,
) {
    let mut direction = RobotDirection::UP;
    let mut position: (i64, i64) = (0, 0);
    let mut color_output: Option<PaintColor> = None;

    processor.run(executable, &mut |signal_input| match signal_input {
        SignalInput::Stdin(input) => {
            let paint_color = *color_map.get(&position).unwrap_or(&PaintColor::Black);
            *input = paint_color.into();
            SignalOutput::Continue
        }
        SignalInput::Stdout(output) => {
            match color_output.take() {
                None => color_output = Some(output.try_into().expect("Invalid paint color!")),
                Some(paint_color) => {
                    color_map.insert(position, paint_color);
                    direction = match output {
                        0 => direction.turn(RobotDirection::LEFT),
                        1 => direction.turn(RobotDirection::RIGHT),
                        _ => panic!("Invalid turn direction"),
                    };

                    position = direction.move_from(position);
                }
            }

            SignalOutput::Continue
        }
        SignalInput::Halt => SignalOutput::Halt,
    });
}

pub fn get_registration(processor: &Processor, executable: Executable) -> String {
    let mut color_map = HashMap::new();
    color_map.insert((0, 0), PaintColor::White);
    paint_map(processor, executable, &mut color_map);

    let x_min_max = color_map.keys().map(|(x, _)| *x).minmax();
    let y_min_max = color_map.keys().map(|(_, y)| *y).minmax();

    let (x_min, x_max) = match x_min_max {
        MinMaxResult::OneElement(x) => (x, x),
        MinMaxResult::MinMax(min, max) => (min, max),
        _ => unreachable!(
            "A white panel is always inserted, so there should be at least one element."
        ),
    };

    let (y_min, y_max) = match y_min_max {
        MinMaxResult::OneElement(y) => (y, y),
        MinMaxResult::MinMax(min, max) => (min, max),
        _ => unreachable!(
            "A white panel is always inserted, so there should be at least one element."
        ),
    };

    (y_min..=y_max)
        .rev()
        .map(|y| {
            (x_min..=x_max)
                .map(|x| {
                    color_map
                        .get(&(x, y))
                        .unwrap_or(&PaintColor::Black)
                        .as_char()
                })
                .join("")
        })
        .join("\n")
}

#[cfg(test)]
mod test {
    use crate::day_11::{get_registration, paint_count};
    use crate::intcode::processor::Processor;
    use crate::intcode::program::Executable;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_11")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let painted = paint_count(&processor, executable);

        assert_eq!(painted, 2268);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_11")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let registration = get_registration(&processor, executable);
        let expected: String = vec![
            "  ##  #### ###  #  # ####   ##  ##  ###    ",
            " #  # #    #  # # #     #    # #  # #  #   ",
            " #    ###  #  # ##     #     # #    #  #   ",
            " #    #    ###  # #   #      # #    ###    ",
            " #  # #    #    # #  #    #  # #  # # #    ",
            "  ##  #### #    #  # ####  ##   ##  #  #   ",
        ]
        .join("\n");

        println!("{}", registration);
        assert_eq!(registration, expected);

        Ok(())
    }
}
