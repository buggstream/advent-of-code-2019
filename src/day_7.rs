use crate::intcode::process::{SignalInput, SignalOutput};
use crate::intcode::processor::Processor;
use crate::intcode::program::Executable;
use async_std::task;
use futures::channel::mpsc::{channel, Receiver};
use futures::executor::block_on;
use futures::future::join_all;
use futures::{SinkExt, StreamExt};
use itertools::Itertools;
use std::convert::TryInto;

pub fn largest_output_signal(
    processor: &Processor,
    executable: &Executable,
    start: i64,
    end: i64,
) -> i64 {
    phase_permutations(start, end)
        .iter()
        .map(|phase_sequence| output_signal(processor, executable, phase_sequence))
        .max()
        .unwrap()
}

fn output_signal(processor: &Processor, executable: &Executable, phase_sequence: &[i64]) -> i64 {
    let mut input = 0_i64;

    for phase in phase_sequence {
        let output = processor.run_piped(executable.clone(), &[*phase, input]);
        input = output[0]
    }

    input
}

pub async fn largest_output_signal_feedback(
    processor: &Processor,
    executable: &Executable,
    start: i64,
    end: i64,
) -> i64 {
    join_all(
        phase_permutations(start, end).iter().map(|phase_sequence| {
            output_signal_feedback_loop(processor, executable, phase_sequence)
        }),
    )
    .await
    .into_iter()
    .max()
    .unwrap()
}

async fn output_signal_feedback_loop(
    processor: &Processor,
    executable: &Executable,
    phase_sequence: &[i64],
) -> i64 {
    let (mut start_sender, mut cur_receiver) = channel::<i64>(5);
    let mut join_handles = Vec::new();
    let mut phase_iter = phase_sequence.iter().cloned();
    start_sender
        .send(phase_iter.next().unwrap())
        .await
        .expect("Couldn't send initial phase to the first amplifier");

    for _ in 0..phase_sequence.len() {
        let (new_receiver, mut handler) = chain_handler(cur_receiver, phase_iter.next()).await;
        cur_receiver = new_receiver;
        let cloned_processor = processor.clone();
        let cloned_executable = executable.clone();
        join_handles.push(task::spawn_blocking(move || {
            // Spawn blocking, because we use block_on in the callback
            cloned_processor.run(cloned_executable, &mut handler);
        }));
    }

    start_sender
        .send(0)
        .await
        .expect("Couldn't send initial message to the first amplifier");

    let mut output = None;

    while let Some(cur) = cur_receiver.next().await {
        output = Some(cur);

        if let Err(_) = start_sender.send(cur).await {
            break;
        }
    }

    // Make sure all amplifiers have quit.
    join_all(join_handles).await;

    output.unwrap()
}

async fn chain_handler(
    mut receiver: Receiver<i64>,
    phase_setting: Option<i64>,
) -> (Receiver<i64>, impl FnMut(SignalInput) -> SignalOutput) {
    let (mut new_sender, new_receiver) = channel::<i64>(5);

    if let Some(phase) = phase_setting {
        new_sender
            .send(phase)
            .await
            .expect("Couldn't send phase setting to the amplifier");
    }

    let handler = move |signal_input: SignalInput| match signal_input {
        SignalInput::Stdout(output) => {
            block_on(new_sender.send(output)).expect("Couldn't send input to the receiver");
            SignalOutput::Continue
        }
        SignalInput::Stdin(input) => {
            *input = block_on(receiver.select_next_some());
            SignalOutput::Continue
        }
        SignalInput::Halt => SignalOutput::Halt,
    };

    (new_receiver, handler)
}

fn phase_permutations(start: i64, end: i64) -> Vec<Vec<i64>> {
    (start..end)
        .permutations(
            (end - start)
                .try_into()
                .expect("Invalid start and end position"),
        )
        .collect()
}

#[cfg(test)]
mod test {
    use crate::day_7::{largest_output_signal, largest_output_signal_feedback};
    use crate::intcode::processor::Processor;
    use crate::intcode::program::Executable;
    use futures::executor::block_on;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_7")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let signal = largest_output_signal(&processor, &executable, 0, 5);

        assert_eq!(signal, 199988);

        Ok(())
    }

    #[test]
    fn puzzle_one_simple() -> Result<(), Box<dyn Error>> {
        let executable = Executable::from(vec![
            3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0,
        ]);
        let processor = Processor::default();
        let signal = largest_output_signal(&processor, &executable, 0, 5);

        assert_eq!(signal, 43210);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_7")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let signal = block_on(largest_output_signal_feedback(
            &processor,
            &executable,
            5,
            10,
        ));

        assert_eq!(signal, 17519904);

        Ok(())
    }

    #[test]
    fn puzzle_two_simple() -> Result<(), Box<dyn Error>> {
        let executable = Executable::from(vec![
            3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001, 28, -1,
            28, 1005, 28, 6, 99, 0, 0, 5,
        ]);
        let processor = Processor::default();
        let signal = block_on(largest_output_signal_feedback(
            &processor,
            &executable,
            5,
            10,
        ));

        assert_eq!(signal, 139629729);

        Ok(())
    }
}
