use crate::intcode::process::{SignalInput, SignalOutput};
use crate::intcode::processor::Processor;
use crate::intcode::program::Executable;
use crate::util::RobotDirection;
use itertools::Itertools;
use std::collections::HashSet;

pub fn scaffolds_sum(processor: &Processor, executable: Executable) -> usize {
    let image = collect_image(processor, executable);
    assert!(image.iter().map(|row| row.len()).all_equal());
    print_map(&image);
    let mut sum = 0;

    for y in 0..image.len() {
        for x in 0..image[y].len() {
            if image[y][x] == '#' && is_intersection(&image, x, y) {
                sum += x * y;
            }
        }
    }

    sum
}

fn is_intersection(image: &Vec<Vec<char>>, x: usize, y: usize) -> bool {
    get_neighbours(image, x, y).len() == 4
}

fn get_neighbours(image: &Vec<Vec<char>>, x: usize, y: usize) -> Vec<(usize, usize)> {
    let mut neighbour_locations = vec![(x, y + 1), (x + 1, y)];

    if y > 0 {
        neighbour_locations.push((x, y - 1));
    }
    if x > 0 {
        neighbour_locations.push((x - 1, y));
    }

    neighbour_locations
        .into_iter()
        .filter(|(neigh_x, neigh_y)| {
            image.get(*neigh_y).map(|row| row.get(*neigh_x)) == Some(Some(&'#'))
        })
        .collect_vec()
}

fn collect_image(processor: &Processor, executable: Executable) -> Vec<Vec<char>> {
    let mut map = Vec::new();
    map.push(Vec::new());
    let mut row_index = 0;

    processor.run(executable, &mut |signal_input| match signal_input {
        SignalInput::Stdin(_) => panic!("Unexpected input request!"),
        SignalInput::Stdout(output) => {
            let cur_char = (output as u8) as char;

            if cur_char == '\n' {
                map.push(Vec::new());
                row_index += 1;
            } else {
                map[row_index].push(cur_char);
            }

            SignalOutput::Continue
        }
        SignalInput::Halt => SignalOutput::Halt,
    });

    map.into_iter().filter(|row| row.len() > 0).collect_vec()
}

#[derive(Debug, Copy, Clone)]
pub enum ScaffoldState {
    Space,
    Unvisited,
    Visited(char),
}

pub struct RescueRobot<'a> {
    position: (usize, usize),
    orientation: RobotDirection,
    to_visit: HashSet<(usize, usize)>,
    map: &'a Vec<Vec<char>>,
}

//impl RescueRobot {}

#[allow(dead_code)]
fn print_map(map: &[Vec<char>]) {
    println!("{}", map.iter().map(|row| row.iter().join("")).join("\n"));
}

#[cfg(test)]
mod test {
    use crate::day_17::scaffolds_sum;
    use crate::intcode::processor::Processor;
    use crate::intcode::program::Executable;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_17")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let output = scaffolds_sum(&processor, executable);

        assert_eq!(output, 6024);

        Ok(())
    }
}
