use std::convert::TryInto;
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::num::TryFromIntError;

pub fn index_2d<X, Y, W, H>(x: X, y: Y, width: W, height: H) -> Result<usize, Box<dyn Error>>
where
    X: TryInto<usize, Error = TryFromIntError>,
    Y: TryInto<usize, Error = TryFromIntError>,
    W: TryInto<usize, Error = TryFromIntError>,
    H: TryInto<usize, Error = TryFromIntError>,
{
    let height = height.try_into()?;
    let width = width.try_into()?;
    let x = x.try_into()?;
    let y = y.try_into()?;

    if x >= width || y >= height {
        return Err("Coordinates out of bounds!".into());
    }

    Ok(y * width + x)
}

pub fn file_to_string(mut file: File) -> std::io::Result<String> {
    let mut result = String::new();
    file.read_to_string(&mut result)?;
    Ok(result)
}

#[derive(Debug, Copy, Clone)]
pub enum RobotDirection {
    UP,
    RIGHT,
    DOWN,
    LEFT,
}

impl RobotDirection {
    pub fn turn(&self, direction: RobotDirection) -> RobotDirection {
        match (self, direction) {
            (RobotDirection::UP, RobotDirection::LEFT) => RobotDirection::LEFT,
            (RobotDirection::RIGHT, RobotDirection::LEFT) => RobotDirection::UP,
            (RobotDirection::DOWN, RobotDirection::LEFT) => RobotDirection::RIGHT,
            (RobotDirection::LEFT, RobotDirection::LEFT) => RobotDirection::DOWN,
            (RobotDirection::UP, RobotDirection::RIGHT) => RobotDirection::RIGHT,
            (RobotDirection::RIGHT, RobotDirection::RIGHT) => RobotDirection::DOWN,
            (RobotDirection::DOWN, RobotDirection::RIGHT) => RobotDirection::LEFT,
            (RobotDirection::LEFT, RobotDirection::RIGHT) => RobotDirection::UP,
            _ => panic!("Invalid turn direction"),
        }
    }
}

pub trait RobotMove<Position> {
    fn move_from(&self, position: Position) -> Position;
}

impl RobotMove<(i64, i64)> for RobotDirection {
    fn move_from(&self, (x, y): (i64, i64)) -> (i64, i64) {
        match self {
            RobotDirection::UP => (x, y + 1),
            RobotDirection::RIGHT => (x + 1, y),
            RobotDirection::DOWN => (x, y - 1),
            RobotDirection::LEFT => (x - 1, y),
        }
    }
}

impl RobotMove<(usize, usize)> for RobotDirection {
    fn move_from(&self, (x, y): (usize, usize)) -> (usize, usize) {
        match self {
            RobotDirection::UP => (x, y + 1),
            RobotDirection::RIGHT => (x + 1, y),
            RobotDirection::DOWN => (x, y - 1),
            RobotDirection::LEFT => (x - 1, y),
        }
    }
}
