#[cfg(test)]
mod test {
    use crate::intcode::processor::Processor;
    use crate::intcode::program::Executable;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_9")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let output = processor.run_piped(executable, &[1]);

        assert_eq!(output, vec![3742852857]);

        Ok(())
    }

    #[test]
    fn puzzle_one_simple_test() -> Result<(), Box<dyn Error>> {
        let executable = Executable::from(vec![
            109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99,
        ]);
        let processor = Processor::default();
        let output = processor.run_piped(executable, &[1]);

        assert_eq!(
            output,
            vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99,]
        );

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_9")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let output = processor.run_piped(executable, &[2]);

        assert_eq!(output, vec![73439]);

        Ok(())
    }
}
