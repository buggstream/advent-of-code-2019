use crate::gravity_system::gravity::MovableBody;
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Object1D {
    position: i32,
    velocity: i32,
}

impl Object1D {
    pub fn new(position: i32) -> Object1D {
        Object1D {
            position,
            velocity: 0,
        }
    }
}

impl MovableBody<i32, i32> for Object1D {
    fn get_position(&self) -> &i32 {
        &self.position
    }

    fn get_velocity(&self) -> &i32 {
        &self.velocity
    }

    fn update_position(&mut self) {
        self.position += self.velocity;
    }

    fn increase_velocity(&mut self, velocity: &i32) {
        self.velocity += velocity;
    }

    fn decrease_velocity(&mut self, velocity: &i32) {
        self.velocity -= velocity;
    }
}

impl Display for Object1D {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "pos={}, vel={}", self.position, self.velocity)
    }
}

impl From<(i32, i32)> for Object1D {
    fn from((position, velocity): (i32, i32)) -> Self {
        Object1D { position, velocity }
    }
}
