use crate::gravity_system::gravity::Gravity;
use std::fmt::{Display, Formatter};
use std::ops::{AddAssign, SubAssign};

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Vector3D {
    x: i32,
    y: i32,
    z: i32,
}

impl Vector3D {
    pub fn new(x: i32, y: i32, z: i32) -> Vector3D {
        Vector3D { x, y, z }
    }

    pub fn absolute_sum(&self) -> i32 {
        self.x.abs() + self.y.abs() + self.z.abs()
    }

    pub fn get_x(&self) -> i32 {
        self.x
    }

    pub fn get_y(&self) -> i32 {
        self.y
    }

    pub fn get_z(&self) -> i32 {
        self.z
    }

    pub fn destructure(&self) -> (i32, i32, i32) {
        (self.x, self.y, self.z)
    }
}

impl Gravity<Vector3D> for Vector3D {
    fn gravity(&self, other: &Self) -> Vector3D {
        let x = self.x.gravity(&other.x);
        let y = self.y.gravity(&other.y);
        let z = self.z.gravity(&other.z);

        Vector3D::new(x, y, z)
    }
}

impl AddAssign for Vector3D {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl SubAssign for Vector3D {
    fn sub_assign(&mut self, rhs: Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl Default for Vector3D {
    fn default() -> Self {
        Vector3D { x: 0, y: 0, z: 0 }
    }
}

impl Display for Vector3D {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "<x={}, y={}, z={}>", self.x, self.y, self.z)
    }
}
