pub trait Gravity<V> {
    fn gravity(&self, other: &Self) -> V;
}

impl Gravity<i32> for i32 {
    fn gravity(&self, other: &Self) -> i32 {
        if self < other {
            1
        } else if self == other {
            0
        } else {
            -1
        }
    }
}

pub trait MovableBody<V: Copy, P: Gravity<V>> {
    #[inline]
    fn get_position(&self) -> &P;

    #[inline]
    fn get_velocity(&self) -> &V;

    #[inline]
    fn update_position(&mut self);

    #[inline]
    fn increase_velocity(&mut self, velocity: &V);

    #[inline]
    fn decrease_velocity(&mut self, velocity: &V);

    #[inline]
    fn apply_gravity(&mut self, other: &mut Self) {
        let gravity_force = self.get_position().gravity(other.get_position());

        self.increase_velocity(&gravity_force);
        other.decrease_velocity(&gravity_force);
    }
}

pub trait MovableBodySystem<V: Copy, P: Gravity<V>, B: MovableBody<V, P>> {
    fn get_bodies(&mut self) -> &mut [B];

    fn update_step(&mut self) {
        let mut body_view = self.get_bodies();

        while let Some((current_body, next_bodies)) = body_view.split_first_mut() {
            for next_body in next_bodies {
                current_body.apply_gravity(next_body);
            }

            body_view = &mut body_view[1..];
        }

        for body in self.get_bodies().iter_mut() {
            body.update_position();
        }
    }
}
