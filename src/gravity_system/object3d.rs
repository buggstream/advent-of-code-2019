use crate::gravity_system::gravity::MovableBody;
use crate::gravity_system::object1d::Object1D;
use crate::gravity_system::vector3d::Vector3D;
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone, PartialEq)]
pub struct Object3D {
    position: Vector3D,
    velocity: Vector3D,
}

impl Object3D {
    pub fn new(position: Vector3D) -> Object3D {
        Object3D {
            position,
            velocity: Vector3D::default(),
        }
    }

    fn potential_energy(&self) -> i32 {
        self.position.absolute_sum()
    }

    fn kinetic_energy(&self) -> i32 {
        self.velocity.absolute_sum()
    }

    pub fn total_energy(&self) -> i32 {
        self.potential_energy() * self.kinetic_energy()
    }
}

impl MovableBody<Vector3D, Vector3D> for Object3D {
    fn get_position(&self) -> &Vector3D {
        &self.position
    }

    fn get_velocity(&self) -> &Vector3D {
        &self.velocity
    }

    fn update_position(&mut self) {
        self.position += self.velocity;
    }

    fn increase_velocity(&mut self, velocity: &Vector3D) {
        self.velocity += *velocity;
    }

    fn decrease_velocity(&mut self, velocity: &Vector3D) {
        self.velocity -= *velocity;
    }
}

impl Display for Object3D {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "pos={}, vel={}", self.position, self.velocity)
    }
}

impl From<Object3D> for (Object1D, Object1D, Object1D) {
    fn from(object3d: Object3D) -> Self {
        let (vel_x, vel_y, vel_z) = object3d.velocity.destructure();
        let (pos_x, pos_y, pos_z) = object3d.position.destructure();

        (
            Object1D::from((pos_x, vel_x)),
            Object1D::from((pos_y, vel_y)),
            Object1D::from((pos_z, vel_z)),
        )
    }
}
