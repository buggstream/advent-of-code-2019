use crate::gravity_system::gravity::MovableBodySystem;
use crate::gravity_system::object1d::Object1D;
use crate::gravity_system::object3d::Object3D;
use crate::gravity_system::vector3d::Vector3D;
use num::Integer;
use regex::Regex;
use std::convert::TryFrom;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::io::Read;

#[derive(Debug, PartialEq, Clone)]
pub struct Object3DSystem {
    objects: Vec<Object3D>,
}

impl Object3DSystem {
    pub fn total_energy(&mut self, steps: u64) -> i32 {
        for _ in 0..steps {
            self.update_step();
        }

        self.objects
            .iter()
            .map(|object3d| object3d.total_energy())
            .sum()
    }

    pub fn repeat_after(&mut self) -> u64 {
        let (x, y, z): (Object1DSystem, Object1DSystem, Object1DSystem) = self.clone().into();

        let x_repeat = x.repeat_after();
        let y_repeat = y.repeat_after();
        let z_repeat = z.repeat_after();

        z_repeat.lcm(&x_repeat.lcm(&y_repeat))
    }
}

impl MovableBodySystem<Vector3D, Vector3D, Object3D> for Object3DSystem {
    fn get_bodies(&mut self) -> &mut [Object3D] {
        &mut self.objects
    }
}

impl TryFrom<File> for Object3DSystem {
    type Error = Box<dyn Error>;

    fn try_from(mut file: File) -> Result<Self, Self::Error> {
        let mut file_input = String::new();
        file.read_to_string(&mut file_input)?;

        let matcher = Regex::new("(?m)^<x=(.+),[[:space:]]*y=(.+),[[:space:]]*z=(.+)>$")?;

        let mut objects3d = Vec::new();

        for capture in matcher.captures_iter(&file_input) {
            objects3d.push(Object3D::new(Vector3D::new(
                (&capture[1]).parse()?, // The match at index 0 is actually the whole string by definition.
                (&capture[2]).parse()?,
                (&capture[3]).parse()?,
            )));
        }

        Ok(Object3DSystem { objects: objects3d })
    }
}

impl Display for Object3DSystem {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        for object3d in self.objects.iter() {
            writeln!(f, "{}", object3d)?
        }

        Ok(())
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Object1DSystem {
    objects: Vec<Object1D>,
}

impl Object1DSystem {
    pub fn repeat_after(mut self) -> u64 {
        let mut steps = 0;
        let initial = self.objects.clone();

        loop {
            steps += 1;
            self.update_step();

            if initial == self.objects {
                break steps;
            }
        }
    }
}

impl MovableBodySystem<i32, i32, Object1D> for Object1DSystem {
    fn get_bodies(&mut self) -> &mut [Object1D] {
        &mut self.objects
    }
}

impl Display for Object1DSystem {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        for object1d in self.objects.iter() {
            writeln!(f, "{}", object1d)?
        }

        Ok(())
    }
}

impl From<Object3DSystem> for (Object1DSystem, Object1DSystem, Object1DSystem) {
    fn from(system: Object3DSystem) -> Self {
        let mut x_objects = Vec::new();
        let mut y_objects = Vec::new();
        let mut z_objects = Vec::new();

        for object3d in system.objects {
            let (x, y, z) = object3d.into();
            x_objects.push(x);
            y_objects.push(y);
            z_objects.push(z);
        }

        (
            Object1DSystem { objects: x_objects },
            Object1DSystem { objects: y_objects },
            Object1DSystem { objects: z_objects },
        )
    }
}
