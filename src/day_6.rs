use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub fn read_orbits(file: File) -> Result<HashMap<String, String>, std::io::Error> {
    let buffered_reader = BufReader::new(file);

    buffered_reader
        .lines()
        .map(|line_result| {
            line_result.map(|line| {
                let mut split = line.split(')');
                let parent = split.next().unwrap().to_owned();
                let child = split.next().unwrap().to_owned();
                (child, parent)
            })
        })
        .collect()
}

pub fn calculate_total_orbits(orbits: &HashMap<String, String>) -> u64 {
    orbits
        .keys()
        .map(|child| get_parents_amount(child, orbits))
        .sum()
}

fn get_parents_amount(child: &str, orbits: &HashMap<String, String>) -> u64 {
    let mut cur_child = child;
    let mut count = 0;

    while let Some(parent) = orbits.get(cur_child) {
        count += 1;
        cur_child = parent;
    }

    count
}

pub fn calculate_transfers(orbits: &HashMap<String, String>) -> Option<usize> {
    let my_parents = get_parents("YOU", orbits);
    let santa_parents = get_parents("SAN", orbits);

    for (santa_index, santa_parent) in santa_parents.into_iter().enumerate() {
        let index = my_parents
            .iter()
            .position(|my_parent| *my_parent == santa_parent);

        if let Some(my_index) = index {
            return Some(santa_index + my_index);
        }
    }

    None
}

fn get_parents<'a>(child: &str, orbits: &'a HashMap<String, String>) -> Vec<&'a String> {
    let mut cur_child = child;
    let mut parents = Vec::new();

    while let Some(parent) = orbits.get(cur_child) {
        parents.push(parent);
        cur_child = parent;
    }

    parents
}

#[cfg(test)]
mod test {
    use crate::day_6::{calculate_total_orbits, calculate_transfers, read_orbits};
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_6")?;
        let orbits_map = read_orbits(data_file)?;
        let orbit_amount = calculate_total_orbits(&orbits_map);

        assert_eq!(orbit_amount, 270768);

        Ok(())
    }

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_6_orbits_count")?;
        let orbits_map = read_orbits(data_file)?;
        let transfer_amount = calculate_total_orbits(&orbits_map);

        assert_eq!(transfer_amount, 42);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_6")?;
        let orbits_map = read_orbits(data_file)?;
        let transfer_amount = calculate_transfers(&orbits_map);

        assert_eq!(transfer_amount, Some(451));

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_6_transfers")?;
        let orbits_map = read_orbits(data_file)?;
        let transfer_amount = calculate_transfers(&orbits_map);

        assert_eq!(transfer_amount, Some(4));

        Ok(())
    }
}
