use std::fs::File;
use std::io::Read;
use std::num::ParseIntError;

pub fn read_number_file(mut file: File) -> Result<Vec<i64>, ParseIntError> {
    let mut file_input = String::new();
    file.read_to_string(&mut file_input).unwrap();
    file_input.lines().map(|line| line.parse()).collect()
}

pub fn total_fuel_requirement(modules: &[i64]) -> i64 {
    modules.iter().map(|module_mass| module_mass / 3 - 2).sum()
}

pub fn recursive_fuel_required(module: i64) -> i64 {
    let fuel = module / 3 - 2;
    if fuel < 0 {
        return 0;
    }

    return fuel + recursive_fuel_required(fuel);
}

pub fn fuel_requirement_including_fuel(modules: &[i64]) -> i64 {
    modules
        .iter()
        .map(|module_mass| recursive_fuel_required(*module_mass))
        .sum()
}

#[cfg(test)]
mod test {
    use crate::day_1::{fuel_requirement_including_fuel, read_number_file, total_fuel_requirement};
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn recursive_total_fuel() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_1")?;
        let input_numbers = read_number_file(data_file)?;
        let rec_fuel = fuel_requirement_including_fuel(&input_numbers);

        assert_eq!(rec_fuel, 4943969);
        Ok(())
    }

    #[test]
    fn normal_total_fuel() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_1")?;
        let input_numbers = read_number_file(data_file)?;
        let rec_fuel = total_fuel_requirement(&input_numbers);

        assert_eq!(rec_fuel, 3297896);

        Ok(())
    }
}
