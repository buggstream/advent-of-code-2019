use std::fs::File;
use std::io::Read;
use std::num::ParseIntError;

pub fn read_opcode_program(mut file: File) -> Result<Vec<usize>, ParseIntError> {
    let mut file_input = String::new();
    file.read_to_string(&mut file_input).unwrap();
    file_input.split(',').map(|number| number.parse()).collect()
}

pub fn run_opcode_program(program: &mut [usize]) {
    let mut program_index = 0;

    loop {
        match program[program_index] {
            1 => {
                program[program[program_index + 3]] =
                    program[program[program_index + 1]] + program[program[program_index + 2]];
                program_index += 4;
            }
            2 => {
                program[program[program_index + 3]] =
                    program[program[program_index + 1]] * program[program[program_index + 2]];
                program_index += 4;
            }
            99 => {
                break;
            }
            _ => unreachable!("Invalid opcode!"),
        }
    }
}

pub fn find_noun_verb(
    program: &[usize],
    (noun_start, noun_end_inclusive): (usize, usize),
    (verb_start, verb_end_inclusive): (usize, usize),
    expected: usize,
) -> Option<(usize, usize)> {
    for noun in noun_start..noun_end_inclusive {
        for verb in verb_start..verb_end_inclusive {
            let mut opcodes = Vec::with_capacity(program.len());
            opcodes.extend_from_slice(program);
            // Overwrite specified values
            opcodes[1] = noun;
            opcodes[2] = verb;

            run_opcode_program(&mut opcodes);

            if opcodes[0] == expected {
                println!("{:?}", opcodes);
                return Some((noun, verb));
            }
        }
    }

    None
}

#[cfg(test)]
mod test {
    use crate::day_2::{find_noun_verb, read_opcode_program, run_opcode_program};
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn run_basic_opcode() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_2")?;
        let mut opcodes = read_opcode_program(data_file)?;
        //    let mut opcodes: Vec<usize> = vec![1,1,1,4,99,5,6,0,99];

        // Overwrite specified values
        opcodes[1] = 12;
        opcodes[2] = 2;

        run_opcode_program(&mut opcodes);

        assert_eq!(opcodes[0], 9581917);

        Ok(())
    }

    #[test]
    fn find_noun_and_verb() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_2")?;
        let opcodes = read_opcode_program(data_file)?;

        let noun_verb = find_noun_verb(&opcodes, (0, 100), (0, 100), 19690720);

        assert_eq!(noun_verb, Some((25, 5)));

        Ok(())
    }
}
