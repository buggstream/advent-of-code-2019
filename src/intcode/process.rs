use crate::intcode::instructions::OpcodeInstruction;
use crate::intcode::parameter::Parameter;
use crate::intcode::program::Executable;
use std::convert::TryInto;

pub type SignalHandler<'a> = &'a mut dyn FnMut(SignalInput) -> SignalOutput;

pub struct Process<'a> {
    program: Executable,
    signal_handler: SignalHandler<'a>,
    signal_output: Option<SignalOutput>,
    instruction_pointer: usize,
    relative_base: i64,
}

impl<'a> Process<'a> {
    pub(super) fn initialize_state(program: Executable, signal_handler: SignalHandler) -> Process {
        Process {
            program,
            signal_handler,
            signal_output: None,
            instruction_pointer: 0,
            relative_base: 0,
        }
    }

    pub fn get_memory(&self) -> &Executable {
        &self.program
    }

    pub fn get_memory_mut(&mut self) -> &mut Executable {
        &mut self.program
    }

    pub fn get_parameters(
        &mut self,
        instruction: i64,
        operation: &OpcodeInstruction,
    ) -> Vec<Parameter> {
        (0..operation.get_param_amount())
            .map(|param_index| {
                let mode = (instruction / 10_i64.pow(param_index + 2)) % 10;
                let index = self.instruction_pointer + param_index as usize + 1;

                match mode {
                    0 => Parameter::Position(self.program.read_pointer(index)),
                    1 => Parameter::Immediate(self.program.read_value(index)),
                    2 => {
                        let offset = self.program.read_value(index);
                        Parameter::Position(self.get_relative_pointer(offset))
                    }
                    _ => panic!("Invalid mode supplied!"),
                }
            })
            .collect()
    }

    pub(super) fn call_signal(&mut self, signal_input: SignalInput) {
        self.signal_output = Some((self.signal_handler)(signal_input));
    }

    pub fn get_signal_output(&self) -> &Option<SignalOutput> {
        &self.signal_output
    }

    pub(super) fn take_signal_output(&mut self) -> Option<SignalOutput> {
        self.signal_output.take()
    }

    pub fn retrieve_instruction(&mut self) -> (i64, i64) {
        let instruction = self.program.read_value(self.instruction_pointer);
        let opcode = instruction % 100;

        (instruction, opcode)
    }

    pub fn increase_pointer(&mut self, amount: usize) {
        self.instruction_pointer += amount;
    }

    pub fn jump_to_pointer(&mut self, location: usize) {
        self.instruction_pointer = location;
    }

    pub fn get_relative_pointer(&self, offset: i64) -> usize {
        (self.relative_base + offset)
            .try_into()
            .expect("Invalid relative pointer value!")
    }

    pub fn adjust_relative_base(&mut self, offset: i64) {
        self.relative_base += offset;
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum SignalInput<'input> {
    Halt,
    Stdout(i64),
    Stdin(&'input mut i64),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum SignalOutput {
    Halt,
    Continue,
}
