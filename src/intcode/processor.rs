use crate::intcode::instructions::*;
use crate::intcode::process::{Process, SignalHandler, SignalInput, SignalOutput};
use crate::intcode::program::Executable;
use std::collections::HashMap;

#[derive(Clone)]
pub struct Processor {
    instructions: HashMap<i64, OpcodeInstruction>,
}

impl Processor {
    pub fn new() -> Processor {
        Processor {
            instructions: HashMap::new(),
        }
    }

    pub fn add_instruction(&mut self, opcode: i64, instruction: OpcodeInstruction) {
        self.instructions.insert(opcode, instruction);
    }

    pub fn run_piped(&self, executable: Executable, mut stdin: &[i64]) -> Vec<i64> {
        let mut stdout = Vec::new();

        self.run(executable, &mut |signal_input| match signal_input {
            SignalInput::Stdout(output) => {
                stdout.push(output);
                SignalOutput::Continue
            }
            SignalInput::Stdin(input) => {
                let cur_input = stdin
                    .get(0)
                    .expect("The program expected more input values than were given");
                *input = *cur_input;
                stdin = &stdin[1..];

                SignalOutput::Continue
            }
            SignalInput::Halt => SignalOutput::Halt,
        });

        stdout
    }

    pub fn run(&self, executable: Executable, signal_handler: SignalHandler) {
        let mut process = Process::initialize_state(executable, signal_handler);

        loop {
            let (instruction, opcode) = process.retrieve_instruction();
            let operation = self.instructions.get(&opcode).expect("Invalid opcode!");
            let params = process.get_parameters(instruction, operation);
            (operation.get_instruction_fn())(&mut process, &params);

            if let Some(SignalOutput::Halt) = process.take_signal_output() {
                break;
            }
        }
    }
}

impl Default for Processor {
    fn default() -> Self {
        let mut instructions = Processor::new();
        instructions.add_instruction(1, create_addition_instruction());
        instructions.add_instruction(2, create_multiply_instruction());
        instructions.add_instruction(3, create_store_input_instruction());
        instructions.add_instruction(4, create_write_to_output_instruction());
        instructions.add_instruction(5, create_store_jump_if_true());
        instructions.add_instruction(6, create_store_jump_if_false());
        instructions.add_instruction(7, create_less_than());
        instructions.add_instruction(8, create_equal_to());
        instructions.add_instruction(9, create_adjust_relative_base());
        instructions.add_instruction(99, create_halt_instruction());
        instructions
    }
}
