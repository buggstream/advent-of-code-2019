use crate::intcode::process::Process;
use std::convert::TryInto;

#[derive(Debug, Copy, Clone)]
pub enum Parameter {
    Position(usize),
    Immediate(i64),
}

impl Parameter {
    pub fn value(&self, process: &mut Process) -> i64 {
        match self {
            Parameter::Immediate(value) => *value,
            Parameter::Position(pointer) => process.get_memory_mut().read_value(*pointer),
        }
    }

    pub fn pointer_value(&self, process: &mut Process) -> usize {
        self.value(process)
            .try_into()
            .expect("Invalid pointer value!")
    }

    pub fn store(&self, process: &mut Process, value: i64) {
        match self {
            Parameter::Position(pointer) => process.get_memory_mut().store_value(*pointer, value),
            _ => panic!("Expected a position to store the value at! Not a value!"),
        }
    }
}
