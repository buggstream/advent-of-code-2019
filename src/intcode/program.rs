use std::convert::{TryFrom, TryInto};
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::num::ParseIntError;

#[derive(Debug, Clone)]
pub struct Executable {
    memory: Vec<i64>,
}

impl TryFrom<File> for Executable {
    type Error = Box<dyn Error>;

    fn try_from(mut file: File) -> Result<Self, Self::Error> {
        let mut file_input = String::new();
        file.read_to_string(&mut file_input)?;
        let memory: Result<Vec<i64>, ParseIntError> =
            file_input.split(',').map(|number| number.parse()).collect();
        Ok(Executable { memory: memory? })
    }
}

impl From<Vec<i64>> for Executable {
    fn from(memory: Vec<i64>) -> Self {
        Executable { memory }
    }
}

impl Executable {
    pub fn read_pointer(&mut self, index: usize) -> usize {
        self.read_value(index)
            .try_into()
            .expect("Invalid pointer as parameter!")
    }

    pub fn read_value(&mut self, index: usize) -> i64 {
        self.reserve_memory_up_to(index);
        self.memory[index]
    }

    pub fn store_value(&mut self, index: usize, value: i64) {
        self.reserve_memory_up_to(index);
        self.memory[index] = value;
    }

    fn reserve_memory_up_to(&mut self, index: usize) {
        if index > self.memory.len() - 1 {
            let extra_needed = index - self.memory.len() + 1;
            self.memory.reserve(extra_needed);

            for _ in 0..extra_needed {
                self.memory.push(0);
            }
        }
    }
}
