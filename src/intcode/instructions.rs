use crate::intcode::parameter::Parameter;
use crate::intcode::process::{Process, SignalInput};
use std::fmt;
use std::fmt::Formatter;
use std::ops::Deref;

pub type InstructionFn = fn(&mut Process, &[Parameter]);

#[derive(Clone)]
pub struct OpcodeInstruction {
    params: u32,
    /// Program, instruction pointer, parameters, input, outputs
    instruction_func: Box<InstructionFn>,
}

impl OpcodeInstruction {
    pub fn new(params: u32, instruction_func: Box<InstructionFn>) -> OpcodeInstruction {
        OpcodeInstruction {
            params,
            instruction_func,
        }
    }

    pub fn get_param_amount(&self) -> u32 {
        self.params
    }

    pub fn get_instruction_fn(&self) -> &InstructionFn {
        self.instruction_func.deref()
    }
}

impl fmt::Debug for OpcodeInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "OpcodeInstruction {{ params: {} }}", self.params)
    }
}

pub fn create_addition_instruction() -> OpcodeInstruction {
    OpcodeInstruction::new(
        3,
        Box::new(|process, params| {
            let sum = params[0].value(process) + params[1].value(process);
            params[2].store(process, sum);
            process.increase_pointer(4);
        }),
    )
}

pub fn create_multiply_instruction() -> OpcodeInstruction {
    OpcodeInstruction::new(
        3,
        Box::new(|process, params| {
            let multiplication = params[0].value(process) * params[1].value(process);
            params[2].store(process, multiplication);
            process.increase_pointer(4);
        }),
    )
}
pub fn create_store_input_instruction() -> OpcodeInstruction {
    OpcodeInstruction::new(
        1,
        Box::new(|process, params| {
            let mut next_input = 0;
            process.call_signal(SignalInput::Stdin(&mut next_input));
            params[0].store(process, next_input);
            process.increase_pointer(2);
        }),
    )
}

pub fn create_write_to_output_instruction() -> OpcodeInstruction {
    OpcodeInstruction::new(
        1,
        Box::new(|process, params| {
            let param_one = params[0].value(process);
            process.call_signal(SignalInput::Stdout(param_one));
            process.increase_pointer(2);
        }),
    )
}

pub fn create_store_jump_if_true() -> OpcodeInstruction {
    OpcodeInstruction::new(
        2,
        Box::new(|process, params| {
            if params[0].value(process) != 0 {
                let location = params[1].pointer_value(process);
                process.jump_to_pointer(location);
            } else {
                process.increase_pointer(3);
            }
        }),
    )
}

pub fn create_store_jump_if_false() -> OpcodeInstruction {
    OpcodeInstruction::new(
        2,
        Box::new(|process, params| {
            if params[0].value(process) == 0 {
                let location = params[1].pointer_value(process);
                process.jump_to_pointer(location);
            } else {
                process.increase_pointer(3);
            }
        }),
    )
}

pub fn create_less_than() -> OpcodeInstruction {
    OpcodeInstruction::new(
        3,
        Box::new(|process, params| {
            if params[0].value(process) < params[1].value(process) {
                params[2].store(process, 1);
            } else {
                params[2].store(process, 0);
            }

            process.increase_pointer(4);
        }),
    )
}

pub fn create_equal_to() -> OpcodeInstruction {
    OpcodeInstruction::new(
        3,
        Box::new(|process, params| {
            if params[0].value(process) == params[1].value(process) {
                params[2].store(process, 1);
            } else {
                params[2].store(process, 0);
            }

            process.increase_pointer(4);
        }),
    )
}

pub fn create_adjust_relative_base() -> OpcodeInstruction {
    OpcodeInstruction::new(
        1,
        Box::new(|process, params| {
            let param_one = params[0].value(process);
            process.adjust_relative_base(param_one);

            process.increase_pointer(2);
        }),
    )
}

pub fn create_halt_instruction() -> OpcodeInstruction {
    OpcodeInstruction::new(
        0,
        Box::new(|process, _params| process.call_signal(SignalInput::Halt)),
    )
}
