#[cfg(test)]
mod test {
    use crate::intcode::processor::Processor;
    use crate::intcode::program::Executable;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_5")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let stdout = processor.run_piped(executable, &[1]);

        assert_eq!(stdout, vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 6731945]);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_5")?;
        let executable = Executable::try_from(data_file)?;
        let processor = Processor::default();
        let stdout = processor.run_piped(executable, &[5]);

        assert_eq!(stdout, vec![9571668]);

        Ok(())
    }

    #[test]
    fn compare_to_eight() -> Result<(), Box<dyn Error>> {
        let executable = Executable::from(vec![
            3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0,
            0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4,
            20, 1105, 1, 46, 98, 99,
        ]);
        // The above example program uses an input instruction to ask for a single number.
        // The program will then output 999 if the input value is below 8, output 1000 if
        // the input value is equal to 8, or output 1001 if the input value is greater than 8.
        // Source: https://adventofcode.com/2019/day/5
        let processor = Processor::default();
        let output_one = processor.run_piped(executable.clone(), &[8]);
        let output_two = processor.run_piped(executable.clone(), &[7]);
        let output_three = processor.run_piped(executable.clone(), &[9]);
        let output_four = processor.run_piped(executable.clone(), &[20]);
        let output_five = processor.run_piped(executable.clone(), &[-15]);

        assert_eq!(output_one, vec![1000]);
        assert_eq!(output_two, vec![999]);
        assert_eq!(output_three, vec![1001]);
        assert_eq!(output_four, vec![1001]);
        assert_eq!(output_five, vec![999]);

        Ok(())
    }
}
