//use advent_of_code_2019::day_13::run_game;
use advent_of_code_2019::day_13::run_game;
use advent_of_code_2019::day_2::{find_noun_verb, read_opcode_program};
use advent_of_code_2019::day_3::{
    fewest_steps_intersection, find_closest_intersection_distance, read_wire_layout,
};
use advent_of_code_2019::day_4::{find_passwords_amount_one, find_passwords_amount_two};
use advent_of_code_2019::intcode::processor::Processor;
use advent_of_code_2019::intcode::program::Executable;
use std::convert::TryFrom;
use std::error::Error;
use std::fs::File;

fn main() -> Result<(), Box<dyn Error>> {
    run_game_day_13()?;
    Ok(())
}

pub fn run_day_2() -> Result<(), Box<dyn Error>> {
    let data_file = File::open("./data/day_2")?;
    let opcode_program = read_opcode_program(data_file)?;

    let noun_verb = find_noun_verb(&opcode_program, (0, 100), (0, 100), 19690720);

    if let Some((noun, verb)) = noun_verb {
        println!(
            "Successfully found the following noun and verb: {}, {}",
            noun, verb
        );
    } else {
        println!("No solution found.");
    }

    Ok(())
}

pub fn run_day_3_1() -> Result<(), Box<dyn Error>> {
    let data_file = File::open("./data/day_3")?;
    let (first_wire, second_wire) = read_wire_layout(data_file)?;

    println!("{:?}", first_wire);
    println!("{:?}", second_wire);

    let min_distance = find_closest_intersection_distance(&first_wire, &second_wire);

    println!("{:?}", min_distance);

    Ok(())
}

pub fn run_day_3_2() -> Result<(), Box<dyn Error>> {
    let data_file = File::open("./data/day_3")?;
    let (first_wire, second_wire) = read_wire_layout(data_file)?;

    println!("{:?}", first_wire);
    println!("{:?}", second_wire);

    let min_combined_steps = fewest_steps_intersection(&first_wire, &second_wire);

    println!("{:?}", min_combined_steps);

    Ok(())
}

pub fn run_day_4_1() {
    let candidates_amount = find_passwords_amount_one(168630, 718098);

    println!("{:?}", candidates_amount);
}

pub fn run_day_4_2() {
    let candidates_amount = find_passwords_amount_two(168630, 718098);

    println!("{:?}", candidates_amount);
}

pub fn run_game_day_13() -> Result<(), Box<dyn Error>> {
    let data_file = File::open("./data/day_13")?;
    let mut executable = Executable::try_from(data_file)?;
    let processor = Processor::default();

    executable.store_value(0, 2); // Play for free hack!

    let score = run_game(&processor, executable, 37, 23);
    println!("Final Score: {:?}", score);

    Ok(())
}
