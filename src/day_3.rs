use std::cmp::{max, min};
use std::error::Error;
use std::fs::File;
use std::io::Read;

#[derive(Debug, Copy, Clone)]
pub struct Point {
    x: i64,
    y: i64,
}

impl Point {
    pub fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }

    pub fn distance_to(&self, other: &Point) -> i64 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }

    pub fn distance_to_origin(&self) -> i64 {
        self.x.abs() + self.y.abs()
    }
}

fn crossed_intersect(
    horizontal_y: i64,
    horizontal_min_x: i64,
    horizontal_max_x: i64,
    vertical_x: i64,
    vertical_min_y: i64,
    vertical_max_y: i64,
) -> Option<Point> {
    if horizontal_y <= vertical_max_y
        && horizontal_y >= vertical_min_y
        && vertical_x <= horizontal_max_x
        && vertical_x >= horizontal_min_x
    {
        return Some(Point {
            x: vertical_x,
            y: horizontal_y,
        });
    }

    None
}

fn line_intersect(
    (start_first, end_first): &(Point, Point),
    (start_second, end_second): &(Point, Point),
) -> Option<Point> {
    if start_first.x == end_first.x {
        // first is vertical
        if start_second.x == end_second.x {
            return None; // lines are parallel completely
        }

        // first is vertical, second is horizontal
        return crossed_intersect(
            start_second.y,
            min(start_second.x, end_second.x),
            max(start_second.x, end_second.x),
            start_first.x,
            min(start_first.y, end_first.y),
            max(start_first.y, end_first.y),
        );
    }

    // first is horizontal

    if start_second.y == end_second.y {
        return None; // lines are parallel completely
    }

    // first is horizontal, second is vertical
    return crossed_intersect(
        start_first.y,
        min(start_first.x, end_first.x),
        max(start_first.x, end_first.x),
        start_second.x,
        min(start_second.y, end_second.y),
        max(start_second.y, end_second.y),
    );
}

fn parse_wire(line: &str) -> Result<Vec<(Point, Point)>, Box<dyn Error>> {
    let mut cur_point = Point { x: 0, y: 0 };

    let mut line_parts = Vec::new();

    for wire_part in line.split(',') {
        let direction = wire_part.chars().take(1).next().ok_or_else(|| {
            let boxed_err: Box<dyn Error> = Box::from(format!("Incorrect input: {}", wire_part));
            boxed_err
        })?;

        let wire_length_str: String = wire_part.chars().skip(1).collect();

        let wire_length: i64 = wire_length_str.parse()?;

        let (start_point, end_point) = match direction {
            'U' => (
                Point {
                    x: cur_point.x,
                    y: cur_point.y + 1,
                },
                Point {
                    x: cur_point.x,
                    y: cur_point.y + wire_length,
                },
            ),
            'R' => (
                Point {
                    x: cur_point.x + 1,
                    y: cur_point.y,
                },
                Point {
                    x: cur_point.x + wire_length,
                    y: cur_point.y,
                },
            ),
            'D' => (
                Point {
                    x: cur_point.x,
                    y: cur_point.y - 1,
                },
                Point {
                    x: cur_point.x,
                    y: cur_point.y - wire_length,
                },
            ),
            'L' => (
                Point {
                    x: cur_point.x - 1,
                    y: cur_point.y,
                },
                Point {
                    x: cur_point.x - wire_length,
                    y: cur_point.y,
                },
            ),
            _ => return Err(Box::from("Invalid direction character!")),
        };

        cur_point = end_point;

        line_parts.push((start_point, end_point));
    }

    Ok(line_parts)
}

pub fn read_wire_layout(
    mut file: File,
) -> Result<(Vec<(Point, Point)>, Vec<(Point, Point)>), Box<dyn Error>> {
    let mut file_input = String::new();
    file.read_to_string(&mut file_input)?;

    let mut iter = file_input.lines();

    let first_line = iter.next().unwrap();
    let second_line = iter.next().unwrap();

    Ok((parse_wire(first_line)?, parse_wire(second_line)?))
}

pub fn find_closest_intersection_distance(
    first_wire: &[(Point, Point)],
    second_wire: &[(Point, Point)],
) -> Option<i64> {
    let mut min_distance = None;

    for first_part in first_wire {
        for second_part in second_wire {
            let intersection = line_intersect(first_part, second_part);

            if let Some(new_point) = intersection {
                if let Some(cur_distance) = min_distance {
                    let new_distance = new_point.distance_to_origin();
                    if new_distance < cur_distance {
                        min_distance = Some(new_distance);
                    }
                } else {
                    min_distance = Some(new_point.distance_to_origin());
                }
            }
        }
    }

    min_distance
}

pub fn fewest_steps_intersection(
    first_wire: &[(Point, Point)],
    second_wire: &[(Point, Point)],
) -> Option<i64> {
    let mut combined_steps = None;
    let mut steps_first_wire = 0_i64;

    for first_part in first_wire {
        let (first_part_start, first_part_end) = first_part;
        let mut steps_second_wire = 0_i64;

        for second_part in second_wire {
            let (second_part_start, second_part_end) = second_part;
            let intersection = line_intersect(first_part, second_part);

            if let Some(ref new_point) = intersection {
                let new_steps_first = first_part_start.distance_to(new_point) + 1;
                let new_steps_second = second_part_start.distance_to(new_point) + 1;
                let new_combined_steps =
                    steps_first_wire + steps_second_wire + new_steps_first + new_steps_second;

                if let Some(cur_combined_steps) = combined_steps {
                    if new_combined_steps < cur_combined_steps {
                        combined_steps = Some(new_combined_steps);
                    }
                } else {
                    combined_steps = Some(new_combined_steps);
                }
            }

            steps_second_wire += second_part_start.distance_to(second_part_end) + 1;
        }

        steps_first_wire += first_part_start.distance_to(first_part_end) + 1;
    }

    combined_steps
}

#[cfg(test)]
mod test {
    use crate::day_3::{
        fewest_steps_intersection, find_closest_intersection_distance, parse_wire, read_wire_layout,
    };
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn intersection_distance() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_3")?;
        let (first_wire, second_wire) = read_wire_layout(data_file)?;

        let min_distance = find_closest_intersection_distance(&first_wire, &second_wire);

        assert_eq!(min_distance, Some(1337));

        Ok(())
    }

    #[test]
    fn fewest_amount_of_steps() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_3")?;
        let (first_wire, second_wire) = read_wire_layout(data_file)?;

        let min_combined_steps = fewest_steps_intersection(&first_wire, &second_wire);

        assert_eq!(min_combined_steps, Some(65356));

        Ok(())
    }

    #[test]
    fn fewest_steps_example_1() -> Result<(), Box<dyn Error>> {
        let first_wire = parse_wire("R75,D30,R83,U83,L12,D49,R71,U7,L72")?;
        let second_wire = parse_wire("U62,R66,U55,R34,D71,R55,D58,R83")?;

        let min_combined_steps = fewest_steps_intersection(&first_wire, &second_wire);

        assert_eq!(min_combined_steps, Some(610));

        Ok(())
    }

    #[test]
    fn fewest_steps_example_2() -> Result<(), Box<dyn Error>> {
        let first_wire = parse_wire("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51")?;
        let second_wire = parse_wire("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")?;

        let min_combined_steps = fewest_steps_intersection(&first_wire, &second_wire);

        assert_eq!(min_combined_steps, Some(410));

        Ok(())
    }
}
