use crate::util::index_2d;
use itertools::Itertools;
use std::cmp::max;
use std::convert::TryFrom;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::io::Read;

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Location {
    Asteroid,
    Empty,
}

impl Location {
    pub fn is_empty(&self) -> bool {
        *self == Location::Empty
    }
}

impl TryFrom<char> for Location {
    type Error = Box<dyn Error>;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(Location::Empty),
            '#' => Ok(Location::Asteroid),
            _ => Err("Can't parse the given character as an asteroid map location.".into()),
        }
    }
}

impl Display for Location {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        let display_char = match self {
            Location::Asteroid => '#',
            Location::Empty => '.',
        };

        write!(f, "{}", display_char)
    }
}

#[derive(Debug, Clone)]
pub struct AsteroidMap {
    width: i64,
    height: i64,
    map: Vec<Location>,
}

impl AsteroidMap {
    pub fn best_monitor_position(&self) -> ((i64, i64), usize) {
        self.map
            .iter()
            .enumerate()
            .filter(|(_, loc)| !loc.is_empty())
            .map(|(index, _)| {
                let x = (index % self.width as usize) as i64;
                let y = (index / self.width as usize) as i64;

                ((x, y), self.amount_visible(x, y))
            })
            .max_by_key(|((_, _), count)| *count)
            .unwrap()
    }

    fn amount_visible(&self, pos_x: i64, pos_y: i64) -> usize {
        self.visible_from_position(pos_x, pos_y)
            .into_iter()
            .filter(|is_visible| *is_visible)
            .count()
    }

    pub fn vaporize_all_order(mut self, pos_x: i64, pos_y: i64) -> Vec<(i64, i64)> {
        let mut total_order = Vec::new();
        let start_index = index_2d(pos_x, pos_y, self.width, self.height).unwrap();
        self.map[start_index] = Location::Empty;

        while self
            .map
            .iter()
            .filter(|location| !location.is_empty())
            .count()
            > 0
        {
            for (coords, index) in self.vaporize_round(pos_x, pos_y) {
                self.map[index] = Location::Empty;
                total_order.push(coords);
            }
        }

        total_order
    }

    fn vaporize_round(&self, pos_x: i64, pos_y: i64) -> impl Iterator<Item = ((i64, i64), usize)> {
        self.visible_from_position(pos_x, pos_y)
            .into_iter()
            .enumerate()
            .filter(|(_, is_vaporized)| *is_vaporized)
            .map(|(index, _)| {
                let x = (index % self.width as usize) as i64;
                let y = (index / self.width as usize) as i64;
                let (x_dir, y_dir) = self.get_base_direction((pos_x, pos_y), (x, y));

                //                println!("{}, {} -> {}, {} :: {}", x, y, x_dir as f64, -y_dir as f64, (x_dir as f64).atan2(y_dir as f64));

                ((x, y), index, (x_dir as f64).atan2(y_dir as f64))
            })
            .sorted_by(|(_, _, angle1), (_, _, angle2)| angle2.partial_cmp(angle1).unwrap())
            .map(|(coords, index, _)| (coords, index))
    }

    fn visible_from_position(&self, pos_x: i64, pos_y: i64) -> Vec<bool> {
        let mut visible = self.map.iter().map(|loc| !loc.is_empty()).collect_vec();

        for index in 0..self.width as usize * self.height as usize {
            let x = (index % self.width as usize) as i64;
            let y = (index / self.width as usize) as i64;

            if x == pos_x && y == pos_y {
                visible[index] = false;
                continue;
            };

            if visible[index] && !self.map[index].is_empty() {
                let (x_dir, y_dir) = self.get_base_direction((pos_x, pos_y), (x, y));
                let mut cur_x = x;
                let mut cur_y = y;

                loop {
                    cur_x += x_dir;
                    cur_y += y_dir;

                    let cur_index = match index_2d(cur_x, cur_y, self.width, self.height) {
                        Ok(cur_index) => cur_index,
                        Err(_) => break,
                    };

                    match visible.get_mut(cur_index) {
                        Some(is_visible) => *is_visible = false,
                        _ => break,
                    }
                }
            }
        }

        visible
    }

    fn get_base_direction(
        &self,
        (start_x, start_y): (i64, i64),
        (end_x, end_y): (i64, i64),
    ) -> (i64, i64) {
        let mut x_dir = end_x - start_x;
        let mut y_dir = end_y - start_y;

        for div in (2..=max(x_dir.abs(), y_dir.abs())).rev() {
            while x_dir % div == 0 && y_dir % div == 0 {
                x_dir /= div;
                y_dir /= div;
            }
        }

        (x_dir, y_dir)
    }
}

impl Display for AsteroidMap {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        let map_string = self
            .map
            .chunks_exact(self.width as usize)
            .map(|row| row.iter().join(""))
            .join("\n");

        write!(
            f,
            "AsteroidMap {{\nwidth: {}, height: {},\nmap:\n{}\n}}",
            self.width, self.height, map_string
        )
    }
}

impl TryFrom<File> for AsteroidMap {
    type Error = Box<dyn Error>;

    fn try_from(mut file: File) -> Result<Self, Self::Error> {
        let mut file_input = String::new();
        file.read_to_string(&mut file_input)?;

        let width = match file_input.lines().next() {
            Some(line) => line.len(),
            None => return Err("The given file doesn't contain any text!".into()),
        };

        let mut all_equal_width = true;

        let map_result: Result<Vec<Location>, Box<dyn Error>> = file_input
            .lines()
            .map(|line| {
                if line.len() != width {
                    all_equal_width = false;
                }

                line.chars()
                    .map(|asteroid_char| Location::try_from(asteroid_char))
            })
            .flatten()
            .collect();

        if !all_equal_width {
            return Err("Not all lines have equal width!".into());
        }

        let map = map_result?;
        let height = (map.len() / width) as i64;

        Ok(AsteroidMap {
            width: width as i64,
            height,
            map,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::day_10::AsteroidMap;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_10")?;
        let map = AsteroidMap::try_from(data_file)?;

        assert_eq!(map.best_monitor_position(), ((22, 19), 282));

        Ok(())
    }

    #[test]
    fn puzzle_one_medium_test() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_10_medium")?;
        let map = AsteroidMap::try_from(data_file)?;

        assert_eq!(map.best_monitor_position(), ((11, 13), 210));

        Ok(())
    }

    #[test]
    fn puzzle_two_math_test() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_10_small_vaporize")?;
        let map = AsteroidMap::try_from(data_file)?;
        let vaporize_order = map.vaporize_all_order(8, 3);

        assert_eq!(vaporize_order.len(), 36);

        let (x, y) = vaporize_order[35]; // 36th element

        assert_eq!(x, 14);
        assert_eq!(y, 3);

        Ok(())
    }

    #[test]
    fn puzzle_two_medium() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_10_medium")?;
        let map = AsteroidMap::try_from(data_file)?;
        let vaporize_order = map.vaporize_all_order(11, 13);
        let (x, y) = vaporize_order[199]; // 200th element

        assert_eq!(x, 8);
        assert_eq!(y, 2);
        assert_eq!(100 * x + y, 802);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_10")?;
        let map = AsteroidMap::try_from(data_file)?;
        let vaporize_order = map.vaporize_all_order(22, 19);
        let (x, y) = vaporize_order[199]; // 200th element

        assert_eq!(100 * x + y, 1008);

        Ok(())
    }
}
