use itertools::Itertools;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::error::Error;
use std::fs::File;
use std::io::Read;

impl TryFrom<&str> for ReactionList {
    type Error = Box<dyn Error>;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let reactions: Result<HashMap<String, (u64, Vec<(String, u64)>)>, Box<dyn Error>> =
            value.lines().map(|line| parse_reaction(line)).collect();

        Ok(ReactionList {
            reactions: reactions?,
        })
    }
}

fn parse_reaction(line: &str) -> Result<(String, (u64, Vec<(String, u64)>)), Box<dyn Error>> {
    let mut split_input_output = line.split("=>");
    let input_str: Result<&str, Box<dyn Error>> =
        split_input_output.next().ok_or("No input given".into());
    let output_str: Result<&str, Box<dyn Error>> =
        split_input_output.next().ok_or("No output given".into());

    let inputs = ingredient_iter(input_str?).collect_vec();
    let output: Result<(String, u64), Box<dyn Error>> = ingredient_iter(output_str?)
        .next()
        .ok_or("No valid output detected".into());

    let (output_name, output_amount) = output?;

    Ok((output_name, (output_amount, inputs)))
}

lazy_static! {
    static ref INGREDIENT_REGEX: Regex = Regex::new(r"(\d+) ([[:alpha:]]+)").unwrap();
}

fn ingredient_iter<'a>(text_input: &'a str) -> impl Iterator<Item = (String, u64)> + 'a {
    INGREDIENT_REGEX.captures_iter(text_input).map(|capture| {
        (
            capture.get(2).unwrap().as_str().to_owned(),
            capture.get(1).unwrap().as_str().parse().unwrap(),
        )
    })
}

#[derive(Debug)]
pub struct ReactionList {
    reactions: HashMap<String, (u64, Vec<(String, u64)>)>,
}

impl ReactionList {
    pub fn produce_fuel_cost(&self, fuel_amount: u64) -> u64 {
        self.produce(String::from("FUEL"), fuel_amount, &mut HashMap::new())
    }

    pub fn produce_max_fuel(&self, ore_available: u64) -> u64 {
        let ore_per_fuel_max = self.produce_fuel_cost(1);
        let mut lower_bound = ore_available / ore_per_fuel_max;
        let mut upper_bound = ore_available + 1;

        loop {
            let length = upper_bound - lower_bound;
            if length <= 1 {
                return lower_bound;
            }

            let mid = length / 2 + lower_bound;
            let mid_cost = self.produce_fuel_cost(mid);

            if mid_cost > ore_available {
                upper_bound = mid;
            } else if mid_cost < ore_available {
                lower_bound = mid;
            } else {
                return mid;
            }
        }
    }

    fn produce(&self, material: String, amount: u64, cached: &mut HashMap<String, u64>) -> u64 {
        if material == "ORE" {
            return amount;
        }

        let needed = match cached.get_mut(&material) {
            Some(created) if *created >= amount => {
                *created -= amount;
                return 0;
            }
            Some(created) => {
                let temp = amount - *created;
                *created = 0;
                temp
            }
            None => amount,
        };

        let (produced, ingredients) = self.reactions.get(&material).unwrap();
        let multiplier = (needed / produced) + (needed % produced > 0) as u64;

        let to_cache = (multiplier * produced) - needed;
        *cached.entry(material).or_insert(0) += to_cache;

        ingredients
            .iter()
            .map(|(ingredient_name, ingredient_amount)| {
                self.produce(
                    ingredient_name.clone(),
                    *ingredient_amount * multiplier,
                    cached,
                )
            })
            .sum()
    }
}

impl TryFrom<File> for ReactionList {
    type Error = Box<dyn Error>;

    fn try_from(mut file: File) -> Result<Self, Self::Error> {
        let mut file_input = String::new();
        file.read_to_string(&mut file_input)?;

        ReactionList::try_from(file_input.as_str())
    }
}

#[cfg(test)]
mod test {
    use crate::day_14::ReactionList;
    use std::convert::TryFrom;
    use std::error::Error;
    use std::fs::File;

    #[test]
    fn puzzle_one() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_14")?;
        let reactions = ReactionList::try_from(data_file)?;

        assert_eq!(reactions.produce_fuel_cost(1), 502491);

        Ok(())
    }

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_14_small")?;
        let reactions = ReactionList::try_from(data_file)?;

        assert_eq!(reactions.produce_fuel_cost(1), 165);

        Ok(())
    }

    #[test]
    fn puzzle_two() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_14")?;
        let reactions = ReactionList::try_from(data_file)?;

        assert_eq!(reactions.produce_max_fuel(1000000000000), 2944565);

        Ok(())
    }

    #[test]
    fn puzzle_two_medium() -> Result<(), Box<dyn Error>> {
        let data_file = File::open("./data/day_14_medium")?;
        let reactions = ReactionList::try_from(data_file)?;

        assert_eq!(reactions.produce_max_fuel(1000000000000), 460664);

        Ok(())
    }
}
