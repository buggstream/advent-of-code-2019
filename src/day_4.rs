pub fn find_passwords_amount_one(start: u64, end: u64) -> usize {
    (start..end).filter(password_check_one).count()
}

pub fn find_passwords_candidates_one(start: u64, end: u64) -> Vec<u64> {
    (start..end).filter(password_check_one).collect()
}

fn password_check_one(number: &u64) -> bool {
    let digits: Vec<u64> = number
        .to_string()
        .chars()
        .map(|digit| digit.to_digit(10).unwrap() as u64)
        .collect();
    let mut adjacent = false;

    for i in 1..digits.len() {
        if digits[i] < digits[i - 1] {
            return false;
        }

        if digits[i] == digits[i - 1] {
            adjacent = true;
        }
    }

    adjacent
}

pub fn find_passwords_amount_two(start: u64, end: u64) -> usize {
    (start..end).filter(password_check_two).count()
}

pub fn find_passwords_candidates_two(start: u64, end: u64) -> Vec<u64> {
    (start..end).filter(password_check_two).collect()
}

fn password_check_two(number: &u64) -> bool {
    let digits: Vec<u64> = number
        .to_string()
        .chars()
        .map(|digit| digit.to_digit(10).unwrap() as u64)
        .collect();
    let mut adjacent = None;
    let mut skipping = false;

    for i in 1..digits.len() {
        if digits[i] < digits[i - 1] {
            return false;
        }

        if digits[i] == digits[i - 1] {
            if adjacent == Some(digits[i]) {
                adjacent = None;
                skipping = true;
            } else if adjacent.is_none() && !skipping {
                adjacent = Some(digits[i]);
            }
        } else {
            skipping = false;
        }
    }

    adjacent.is_some()
}

#[cfg(test)]
mod test {
    use crate::day_4::{
        find_passwords_amount_one, find_passwords_amount_two, password_check_one,
        password_check_two,
    };

    #[test]
    fn puzzle_one() {
        let candidates_amount = find_passwords_amount_one(168630, 718098);

        assert_eq!(candidates_amount, 1686);
    }

    #[test]
    fn puzzle_two() {
        let candidates_amount = find_passwords_amount_two(168630, 718098);

        assert_eq!(candidates_amount, 1145);
    }

    #[test]
    fn criteria_one() {
        assert!(password_check_one(&111111));
        assert!(!password_check_one(&223450));
        assert!(!password_check_one(&123789));
    }

    #[test]
    fn criteria_two() {
        assert!(password_check_two(&112233));
        assert!(password_check_two(&111122));
        assert!(!password_check_two(&111111));
        assert!(!password_check_two(&123444));
    }
}
